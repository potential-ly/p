package cmd

import (
	"bufio"
	"fmt"
	"os"
	"os/user"
	"path/filepath"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/iam"
	"github.com/aws/aws-sdk-go/service/sts"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gopkg.in/ini.v1"
)

// authCmd represents the auth command
var authCmd = &cobra.Command{
	Use:   "auth",
	Short: "Generate AWS Short Lived Tokens",
	Long:  `This command will use your AWS keys to generate and export short lived tokens that you can use to access the account.`,
	Run:   runAuth,
}

func init() {
	awsCmd.AddCommand(authCmd)
}

func runAuth(cmd *cobra.Command, args []string) {
	key := viper.GetString("aws-secret-access-key")
	if key == "" {
		fmt.Println("No AWS_SECRET_ACCESS_KEY found in config. Please run `p init`")
		return
	}
	id := viper.GetString("aws-access-key-id")
	if id == "" {
		fmt.Println("No AWS_ACCESS_KEY_ID found in config. Please run `p init`")
		return
	}

	sess := session.New(&aws.Config{
		Credentials: credentials.NewStaticCredentials(id, key, ""),
	})

	mfas, err := getMFA(sess)
	if err != nil {
		fmt.Println(err)
		return
	}

	if len(mfas) == 0 {
		fmt.Println("You don't have MFA configured")
		return
	}

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("MFA token: ")
	token, _ := reader.ReadString('\n')

	svc := sts.New(sess)
	input := &sts.GetSessionTokenInput{
		DurationSeconds: aws.Int64(129600),
		SerialNumber:    aws.String(*mfas[0].SerialNumber),
		TokenCode:       aws.String(strings.TrimSuffix(token, "\n")),
	}

	result, err := svc.GetSessionToken(input)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	usr, err := user.Current()
	if err != nil {
		fmt.Println(err)
		return
	}
	err = os.MkdirAll(filepath.Join(usr.HomeDir, ".aws"), os.ModePerm)
	if err != nil {
		fmt.Println(err)
		return
	}
	credsPath := filepath.Join(usr.HomeDir, ".aws/credentials")
	if _, err := os.Stat(credsPath); os.IsNotExist(err) {
		f, err := os.Create(credsPath)
		if err != nil {
			fmt.Println(err)
			return
		}
		f.Close()
	}

	cfg, err := ini.Load(credsPath)
	if err != nil {
		fmt.Println(err)
		return
	}
	cfg.Section("default").Key("aws_access_key_id").SetValue(*result.Credentials.AccessKeyId)
	cfg.Section("default").Key("aws_secret_access_key").SetValue(*result.Credentials.SecretAccessKey)
	cfg.Section("default").Key("aws_session_token").SetValue(*result.Credentials.SessionToken)
	err = cfg.SaveTo(credsPath)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func getMFA(s *session.Session) ([]*iam.MFADevice, error) {
	svc := iam.New(s)
	input := &iam.ListMFADevicesInput{}

	result, err := svc.ListMFADevices(input)
	if err != nil {
		return nil, err
	}
	return result.MFADevices, nil
}
