package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// initCmd represents the init command
var (
	secretKey string
	keyID     string
	initCmd   = &cobra.Command{
		Use:   "init",
		Short: "Initialise P config",
		Long:  `Creates P config based on provided values.`,
		Run:   initConfig,
	}
)

func init() {
	rootCmd.AddCommand(initCmd)
	initCmd.Flags().StringVarP(&secretKey, "aws-secret-access-key", "k", "", "AWS_SECRET_ACCESS_KEY value")
	initCmd.MarkFlagRequired("aws-secret-access-key")
	initCmd.Flags().StringVarP(&keyID, "aws-access-key-id", "i", "", "AWS_ACCESS_KEY_ID value")
	initCmd.MarkFlagRequired("aws-access-key-id")
}

func initConfig(cmd *cobra.Command, args []string) {
	viper.Set("aws-secret-access-key", secretKey)
	viper.Set("aws-access-key-id", keyID)
	err := viper.WriteConfig()
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("Config saved to: %s\n", viper.ConfigFileUsed())
}
