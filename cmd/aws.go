package cmd

import (
	"github.com/spf13/cobra"
)

// awsCmd represents the aws command
var awsCmd = &cobra.Command{
	Use:   "aws",
	Short: "A set of comands that interact with aws",
	Long:  `A set of comands that interact with aws`,
}

func init() {
	rootCmd.AddCommand(awsCmd)
}
