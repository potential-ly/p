package main

import (
	"bitbucket.org/potential-ly/p/cmd"
)

func main() {
	cmd.Execute()
}
