# P

## P is Potentially utility CLI


This CLI automates multiple different chores that you usually do when interacting with Potantially infrastructure

## How to install

On macOS
```
brew tap potentially-limited/homebrew-tap
brew install p
```

on any other OS

```
go install -v bitbucket.org/potential-ly/p
```

## Usage

```
Usage:
  p [command]

Available Commands:
  aws         A set of comands that interact with aws
  help        Help about any command

Flags:
  -h, --help   help for p

Use "p [command] --help" for more information about a command.
```

## How to authenticate with MFA

```
p aws auth
```

Your creds are going to be saved in `~/.aws/credentials` so you have to unset your `AWS_*` env variables to make it work.
```
unset $(env | awk -F'=' '$1~/AWS_*/ {print $1}')
```
